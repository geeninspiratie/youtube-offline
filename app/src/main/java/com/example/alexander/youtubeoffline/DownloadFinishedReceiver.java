package com.example.alexander.youtubeoffline;

import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DownloadFinishedReceiver extends BroadcastReceiver {


    private static final String TEMP_FILE_NAME = "tmp-";
    private static final Pattern ARTIST_TITLE_PATTERN =
            Pattern.compile("(.+?)(\\s*?)-(\\s*?)(\"|)(\\S(.+?))\\s*?([\\&\\*+,-/:;<=>@_\\|]+?\\s*?|)(\\z|\"|\\(|\\[|lyric|official)",
                    Pattern.CASE_INSENSITIVE | Pattern.UNICODE_CASE);

    @Override
    public void onReceive(final Context context, Intent intent) {
        String action = intent.getAction();
        if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
            Bundle extras = intent.getExtras();
            DownloadManager.Query q = new DownloadManager.Query();
            long downloadId = extras.getLong(DownloadManager.EXTRA_DOWNLOAD_ID);
            q.setFilterById(downloadId);
            Cursor c = ((DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE)).query(q);
            if (c.moveToFirst()) {
                int status = c.getInt(c.getColumnIndex(DownloadManager.COLUMN_STATUS));
                if (status == DownloadManager.STATUS_SUCCESSFUL) {
                    String inPath = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME));
                    String dlTitle = c.getString(c.getColumnIndex(DownloadManager.COLUMN_TITLE));
                    c.close();
                    DownloadStatus dlStatus = getMultiFileDlStatus(context, downloadId, inPath);
                    if (dlStatus != null && dlStatus.readyToMerge) {
                        if (!dlStatus.hasVideo) {
                            String artist = null;
                            String title = null;
                            Matcher mat = ARTIST_TITLE_PATTERN.matcher(dlTitle);
                            if (mat.find()) {
                                artist = mat.group(1);
                                title = mat.group(5);
                            }
                            //convertM4a(inPath, title, artist);
                            scanFile(inPath, context);
                        } else {
                            if (inPath.endsWith(".mp4")) {
                                //mergeMp4(dlStatus.otherFilePath, inPath);
                                String x ="" ;
                                Log.d("TAG", "onReceive: ");
                            } else if (inPath.endsWith(".m4a")) {
                                //mergeMp4(inPath, dlStatus.otherFilePath);
                            }
                        }
                    }
                } else if (status == DownloadManager.STATUS_FAILED) {
                    removeTempOnFailure(context, downloadId);
                }
            }

        }
    }

    private void removeTempOnFailure(Context con, long downloadId) {
        File cacheFileDir = new File(con.getCacheDir().getAbsolutePath());
        for (File f : cacheFileDir.listFiles()) {
            if (f.getName().contains(downloadId + "")) {
                f.delete();
                break;
            }
        }
    }

    private DownloadStatus getMultiFileDlStatus(Context con, long downloadId, String filePath) {
        File cacheFileDir = new File(con.getCacheDir().getAbsolutePath());
        File cacheFile = null;
        for (File f : cacheFileDir.listFiles()) {
            if (f.getName().contains(downloadId + "")) {
                cacheFile = f;
                break;
            }
        }
        if (cacheFile != null && cacheFile.exists()) {
            DownloadStatus dlStatus = new DownloadStatus();
            dlStatus.hasVideo = cacheFile.getName().contains("-");
            BufferedReader reader = null;
            BufferedWriter writer = null;
            try {
                reader = new BufferedReader(new InputStreamReader(new FileInputStream(cacheFile), "UTF-8"));
                dlStatus.otherFilePath = reader.readLine();
                reader.close();
                if (dlStatus.otherFilePath != null || !dlStatus.hasVideo) {
                    cacheFile.delete();
                    dlStatus.readyToMerge = true;
                } else {
                    writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(cacheFile)));
                    writer.write(filePath);
                }
                return dlStatus;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        return null;
    }

    private void scanFile(String path, Context con) {
        File file = new File(path);
        Intent scanFileIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(file));
        con.sendBroadcast(scanFileIntent);
    }

    private class DownloadStatus {
        String otherFilePath;
        boolean readyToMerge = false;
        boolean hasVideo;
    }
}
